﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts {
    class ShootFarthestStrategy : IShootingStrategy {
        public ITargetable GetTarget(List<ITargetable> shootables, Vector3 shootingPosition) {
            ITargetable currentTarget = shootables[0];
            for (int i = 1; i < shootables.Count; i++) {
                if (Vector3.Distance(shootables[i].GetPosition(), shootingPosition) >
                    Vector3.Distance(currentTarget.GetPosition(), shootingPosition)) {
                    currentTarget = shootables[i];
                }
            }
            return currentTarget;
        }
    }
}
