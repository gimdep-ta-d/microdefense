﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts {
    public interface IShootable {
        void ShootAtFrom(ITargetable target, Vector3 sourcePosition, Quaternion sourceRotation, float damage);
    }
}
