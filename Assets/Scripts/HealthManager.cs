﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour {

    public int health;
    public Text text;
    public GameObject gameOverPanel;

    static HealthManager instance;
    public static HealthManager Instance { get { return instance; } }

    // Use this for initialization
    void Start () {
        if (instance && instance != this) {
            Destroy(gameObject);
        } else {
            instance = this;
        }
	}
	
	// Update is called once per frame
	void Update () {
        text.text = health.ToString();
        if (health <= 0 && GridManager.Instance.state != GridState.Dead) {
            gameOverPanel.SetActive(true);
            Time.timeScale = 0f;
            Debug.Log(Time.timeScale);
            ObtainProteins();
            GridManager.Instance.state = GridState.Dead;
        }
    }

    public void ObtainProteins()
    {
        WaveController wc = GameObject.Find("WaveController").GetComponent<WaveController>();
        Text prot = GameObject.Find("Protein Text").GetComponentInChildren<Text>();
        Debug.Log(wc.wave);
        int proteinGain = Mathf.FloorToInt((wc.wave-1) * 1.25f);
        prot.text = System.String.Format("Gained {0} proteins", proteinGain);
        GameManager.Instance.protein += proteinGain;
        GameManager.Instance.SavePlayerData();
    }

    public void reduceHealth(int value) {
        if(health > 0)
            health -= value;
    }
}
