﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class ObjectPoolItem
{    
    public GameObject objectToPool;
    public int amount;
    public bool shouldExpand = true;
}

public class ObjectPooler : MonoBehaviour {


    public static ObjectPooler Instance;

    public List<GameObject> pool;
    public List<ObjectPoolItem> poolableItems;
    
	// Use this for initialization
	void Start () {
        pool = new List<GameObject>();
        foreach(ObjectPoolItem item in poolableItems)
        {
            for (int i = 0; i < item.amount; i++)
            {
                GameObject obj = (GameObject)Instantiate(item.objectToPool);
                obj.SetActive(false);
                pool.Add(obj);
                obj.transform.SetParent(transform);
            }
        }

        
	}

    private void Awake()
    {
        Instance = this;
    }

    public GameObject GetPooledObject(string tag)
    {
        for(int i =0; i < pool.Count; i++)
        {
            if (!pool[i].activeInHierarchy && pool[i].tag == tag)
                return pool[i];
        }

        foreach(ObjectPoolItem item in poolableItems)
        {
            if (item.objectToPool.tag == tag)
            {
                if (item.shouldExpand)
                {
                    GameObject obj = (GameObject)Instantiate(item.objectToPool);
                    obj.SetActive(false);
                    pool.Add(obj);
                    obj.transform.SetParent(transform);
                    return obj;
                }
            }       
        }
        return null;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
