﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveController : MonoBehaviour {

    public EnemySpawner es;
    public GameObject waypointController;
    public int wave = 1;
    public float spawnRate = 1f;
    public Slider progressBar;
    public int monsterIndex = 0;
    public int bossIndex = 0;
    public bool waveOnGoing = false;
    public Text startWaveText;
    public int bossWaveOccurance = 5;
    public int bossOnlyWaves = 3;

    bool spawning = false;
    bool waveProgressing = true;
    float maxWaveTime;
    float currentWaveTime;

    System.Random r;

	// Use this for initialization
	void Start () {
        r = new System.Random();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.S) && !spawning) {
            StartWave();
        }

        if (!IsEnemyExistOnPlay()) {
            waveOnGoing = false;
        } else {
            waveOnGoing = true;
        }
        
        if (waveProgressing && waveOnGoing) {
            currentWaveTime -= Time.deltaTime;

            if (currentWaveTime <= 0f) {
                waveProgressing = false;
            }

            float progress = Mathf.Clamp01(currentWaveTime / maxWaveTime);
            progressBar.value = progress;
        } else {
            WaveProgressManager waveController = progressBar.gameObject.GetComponent<WaveProgressManager>();
            startWaveText.gameObject.SetActive(true);
            waveController.changeColor(Color.green);
            waveController.changeText("Break");
            progressBar.value = 1f;
        }
	}

    private float GetWaveAddedTime() {
        int childNumber = waypointController.transform.childCount;
        float totalDistance = 0f;
        for (int i = 0; i < childNumber - 1; i++) {
            totalDistance += Vector3.Distance(
                waypointController.transform.GetChild(i).position, 
                waypointController.transform.GetChild(i + 1).position
             );
        }
        return totalDistance / es.monsterPrefabs[monsterIndex].GetComponent<Monster>().speed;
    }

    private float GetWaveTime(int wave) {
        return EnemyNumberFunction(wave) * spawnRate;
    }

    IEnumerator SpawnWave(int wave) {
        if(wave > 2)
            monsterIndex = r.Next(0, es.monsterPrefabs.Count);
        if (IsBossWave(wave) && IsBossOnlyWave(wave)) {
            es.SpawnBoss(bossIndex, wave);
            yield return new WaitForSeconds(spawnRate);
        } else if (IsBossWave(wave) && !IsBossOnlyWave(wave)) {
            int enemyNumber = EnemyNumberFunction(wave);
            for (int i = 0; i < enemyNumber; i++) {
                es.Spawn(monsterIndex, wave);
                yield return new WaitForSeconds(spawnRate);
            }

            es.SpawnBoss(bossIndex, wave);
            yield return new WaitForSeconds(spawnRate);
        } else {
            int enemyNumber = EnemyNumberFunction(wave);
            for (int i = 0; i < enemyNumber; i++) {
                es.Spawn(monsterIndex, wave);
                yield return new WaitForSeconds(spawnRate);
            }
        }
        spawning = false;
    }

    int EnemyNumberFunction(int waveNumber) {
        return waveNumber+4;
    }

    public void StartWave() {
        if (!spawning) {
            StartCoroutine(SpawnWave(wave));
            WaveProgressManager waveController = progressBar.gameObject.GetComponent<WaveProgressManager>();
            startWaveText.gameObject.SetActive(false);
            waveController.changeColor(Color.red);
            waveController.changeText("Wave : " + wave);
            maxWaveTime = GetWaveTime(wave) + GetWaveAddedTime();
            currentWaveTime = maxWaveTime;
            spawning = true;
            waveProgressing = true;
            wave += 1;
        }
    }

    private bool IsEnemyExistOnPlay() {
        GameObject[] enemys = GameObject.FindGameObjectsWithTag("Monster");
        if (enemys.Length <= 0) {
            return false;
        } else {
            return true;
        }
    }

    private bool IsBossWave(int wave) {
        return wave % bossWaveOccurance == 0;
    }

    private bool IsBossOnlyWave(int wave) {
        return wave / bossWaveOccurance <= bossOnlyWaves;
    }
}
