﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour, IShootable {

    public Collider2D col;
    public float damage;

    public Vector3 lastDestination;
    public ITargetable target;
    public float speed;
	// Use this for initialization
	void Start () {
        Debug.Log("I am a bullet");
	}

    public void ShootAtFrom(ITargetable shootTarget, Vector3 sourcePosition, Quaternion sourceRotation, float damageGiven) {
        gameObject.SetActive(true);
        target = shootTarget;
        damage = damageGiven;
        transform.rotation = sourceRotation;
        transform.position = sourcePosition;
    }

    // Update is called once per frame
    void Update () {

        Debug.Log(transform.position);
        if (transform.position == lastDestination)
        {
            //target.GetComponent<HPController>().Hit(damage);
            //GameObject temp = Instantiate(dummyAudioPrefab, transform.position, Quaternion.identity);
            //temp.GetComponent<DummyAudio>().PlayClip(hitSound);
            gameObject.SetActive(false);
        }

        if (!target.IsAlive()) 
        {
            gameObject.SetActive(false);
            Debug.Log("Null monster at bullet.cs");
        }
        else 
        {

            //Debug.Log(new System.Random().Next() + " Target ID: " + target.GetId() + " " + target.IsAlive());
            lastDestination = target.GetPosition();
            transform.position = Vector3.MoveTowards(transform.position, target.GetPosition(), Time.deltaTime * speed);
            if (transform.position == target.GetPosition()) {
                gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Monster" && other.GetComponent<Monster>().GetId() == target.GetId())
        {
            //Debug.Log("Hit monster #" + target.GetId());
            other.gameObject.GetComponent<Monster>().Damage(damage);
            gameObject.SetActive(false);
            return;
        }
    }

}
