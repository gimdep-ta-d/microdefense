﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GridState
{
    Build,
    Move,
    Standby,
    Select,
    Dead
}

public class GridManager : MonoBehaviour
{

    static GridManager instance;
    public static GridManager Instance { get { return instance; } }
    public GameManager gm;

    public Dictionary<TilePoint, Tile> tiles = new Dictionary<TilePoint, Tile>();

    public Tower towerPrefab;

    public GridState state;

    public bool baseTower = false;

    public Tower selectedTower;
    public bool isMovingTower;

    public Text cloneCostText;
    public Text levelUpCostText;

    private void Start()
    {

    }

    void Awake()
    {
        if (instance && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        state = GridState.Standby;

        for(int i = 0; i < transform.childCount; i++)
        {
            Tile child = transform.GetChild(i).GetComponent<Tile>();
            //Debug.Log(child + " " + child.pos);
            tiles.Add(child.pos, child);
        }

    }

    public List<Tile> FindNeighbors(Tile center)
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            Tile t = transform.GetChild(i).GetComponent<Tile>();
            t.sr.color = Color.white;
        }
        //Debug.Log(center);
        center.sr.color = new Color(0, 1, 0);
        List<Tile> neighbors = new List<Tile>();
        for(int i = -1; i <= 1; i++)
        {
            for(int j = -1; j <= 1; j++)
            {
                TilePoint p = new TilePoint(center.x + i, center.y + j);
                Tile temp = null;
                if (tiles.TryGetValue(p, out temp) && p != center.pos)
                {
                    if(temp.type == TileType.Build)
                    {
                        neighbors.Add(temp);
                        if (temp.CanPlaceTower())
                        {
                            temp.sr.color = new Color(0, 0, 1);
                        }
                        else
                        {
                            temp.sr.color = new Color(1, 0, 0);
                        }
                    }                    
                }
            }
        }
        return neighbors;
    }

    public void PlaceTower(Tile tile)
    {
        switch (state)
        {
            case GridState.Build:
                //Debug.Log(GameManager.Instance.energy);
                if (tile.CanPlaceTower() && selectedTower && selectedTower.cost <= EnergyManager.Instance.energy)
                {
                    Tower towerClone = Instantiate(selectedTower, new Vector3(tile.transform.position.x, tile.transform.position.y, tile.transform.position.z - 0.1f), Quaternion.identity);
                    Debug.Log(selectedTower.gameObject.name);
                    //selectedTower.gameObject.SetActive(false);

                    tile.t = towerClone;
                    towerClone.tile = tile;
                    //tile.t.transform.SetParent(tile.transform);
                    towerClone.gameObject.SetActive(true);
                    EnergyManager.Instance.UseEnergy(towerClone.cost);
                    DisableButton(selectedTower);
                    state = GridState.Standby;
                    InGameTextManager.Instance.HideBuildTowerInfo();
                    InGameTextManager.Instance.UpdateTowerInfo(towerClone);
                }
                break;

            case GridState.Move:
                if(tile.CanPlaceTower() && selectedTower)
                {
                    Debug.Log("move tower pls");
                    selectedTower.tile.t = null;
                    selectedTower.tile = tile;
                    tile.t = selectedTower;
                    selectedTower.transform.position = new Vector3(tile.transform.position.x, tile.transform.position.y, tile.transform.position.z - 0.1f);
                    //selectedTower.transform.SetParent(tile.transform);                    
                    isMovingTower = false;
                    state = GridState.Standby;
                }
                break;

            default:
                break;

        }
    }

    public void DuplicateTower()
    {
        if (selectedTower)
        {
            //GameManager.Instance.UseEnergy(selectedTower.cost);
            selectedTower.DuplicateTower();
        }
    }

    public void LevelUpTower()
    {
        if(selectedTower)
            selectedTower.LevelUp();
    }

    public void MoveTower()
    {
        if (selectedTower)
            state = GridState.Move;
        Debug.Log(state);
    }

    void DisableButton(Tower t)
    {
        Debug.Log("Disabling button");
        foreach(TowerButton b in TowerManager.Instance.buttons)
        {
            Debug.Log(b.name);
            Debug.Log(t.name);
            if (b.designatedTower == t)
            {
                b.gameObject.SetActive(false);
                return;
            }                
        }
    }
}
