﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TileType
{
    Path,
    Void,
    Build
}

public enum TileName
{
    UpRightCorner,
    UpCenter,
    UpLeftCorner,
    CenterRight,
    Center,
    CenterLeft,
    DownRightCorner,
    DownCenter,
    DownLeftCorner
}

[ExecuteInEditMode]
public class Tile : MonoBehaviour {

    public Collider2D col;
    public TilePoint pos;
    public GridManager gm;
    public List<Tile> neighbors = new List<Tile>();
    public Tower t;
    public TileType type;
    public TileName tileName;
    public SpriteRenderer sr;

    public List<Sprite> tileSprites;
	// Use this for initialization
	void Start () {
		
	}

    public int x { get { return pos.x; } }
    public int y { get { return pos.y; } }

    private void Awake()
    {
        pos.x = (int) transform.position.x;
        pos.y = (int)transform.position.y;
        //Debug.Log(this + " " + pos);
        gm = this.transform.GetComponentInParent<GridManager>();
        UpdateTileSprite();
    }

    // Update is called once per frame
    void Update () {
		if(Application.isEditor && !Application.isPlaying)
        {
            pos.x = (int)transform.position.x;
            pos.y = (int)transform.position.y;
            sr.color = Color.white;
            UpdateTileSprite();
            /*switch (type)
            {
                case TileType.Build:
                    sr.color = new Color(0, 1, 0);
                    break;
                case TileType.Path:
                    sr.color = new Color(1, 1, 1);
                    break;
                case TileType.Void:
                    sr.color = new Color(1, 0, 0);
                    break;
            }*/
        }
	}
    
    public bool CanPlaceTower()
    {
        return (type == TileType.Build && t == null);
    }

    private void OnMouseUp()
    {
        Debug.Log("I'm a tile on position: " + pos.x + " " + pos.y);
        if (t == null) {
            InGameTextManager.Instance.HideTowerInfo();
        }
        gm.FindNeighbors(this);
        gm.PlaceTower(this);
        gm.selectedTower = null;
        gm.isMovingTower = false;
    }

    private void UpdateTileSprite()
    {
        switch (type)
        {
            case (TileType.Path):
                sr.sprite = tileSprites[12];
                break;
            case (TileType.Build):
                switch (tileName)
                {
                    case (TileName.DownCenter):
                        sr.sprite = tileSprites[22];
                        break;
                    case (TileName.DownLeftCorner):
                        sr.sprite = tileSprites[20];
                        break;
                    case (TileName.DownRightCorner):
                        sr.sprite = tileSprites[24];
                        break;
                    case (TileName.UpCenter):
                        sr.sprite = tileSprites[2];
                        break;
                    case (TileName.UpLeftCorner):
                        sr.sprite = tileSprites[0];
                        break;
                    case (TileName.UpRightCorner):
                        sr.sprite = tileSprites[4];
                        break;
                    case (TileName.CenterLeft):
                        sr.sprite = tileSprites[10];
                        break;
                    case (TileName.CenterRight):
                        sr.sprite = tileSprites[14];
                        break;
                }
                break;
        }
    }
    
}