﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TowerButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    public Tower designatedTower;
    public Button button;
    public Image image;
    public SpriteRenderer sr;
    public InGameTextManager textManager;

    private float blinkTimer = 0; //blink gap
    private float blinkDuration = 0; //span of blinking
    private float blinkPeriod;
    private bool isBlinkingUp = true;
    private Color imageOriginalColor;

	// Use this for initialization
	void Start () {
        blinkPeriod = new System.Random().Next(50, 100) / 1000f;
        textManager = InGameTextManager.Instance;
    }

    private void Awake()
    {
        button = GetComponent<Button>();
        image = GetComponent<Image>();
        imageOriginalColor = image.color;
    }

    // Update is called once per frame
    void Update () {
        blink();
	}

    void blink() {
        if (image) {
            if (blinkDuration < 20f) {
                if (blinkTimer > blinkPeriod) {
                    float blue = image.color.b + (isBlinkingUp ? .1f : -.1f);
                    if (blue > .5f) {
                        isBlinkingUp = false;
                    }
                    else if (blue < 0f) {
                        isBlinkingUp = true;
                        blue = 0;
                    }
                    image.color = new Color(image.color.r, image.color.g, blue);
                    blinkTimer = 0;
                }
                blinkDuration += Time.deltaTime;
                blinkTimer += Time.deltaTime;
            }
            else if (blinkDuration < 30f) {
                image.color = imageOriginalColor;
                blinkDuration += 99f;
            }
        }
    }

    void UpdateDisplay()
    {
        image.sprite = sr.sprite;
        //image.color = sr.color;
    }

    public void DesignateTower(Tower t)
    {
        designatedTower = t;
        sr = designatedTower.sr;
        UpdateDisplay();
    }

    public void SelectTower()
    {
        if (EnergyManager.Instance.energy < designatedTower.cost) {
            textManager.DisplayBuildTowerInsufficientEnergy(designatedTower);
        }
        else {
            Debug.Log("You have selected tower: " + designatedTower.gameObject.name);
            GridManager.Instance.selectedTower = designatedTower;
            GridManager.Instance.state = GridState.Build;
            textManager.DisplayBuildTowerBuilding(designatedTower);
        }
    }

    public void OnPointerEnter(PointerEventData data) {
        if (designatedTower != null) {
            textManager.DisplayBuildTowerCost(designatedTower);
        }
    }

    public void OnPointerExit(PointerEventData data) {
        textManager.HideBuildTowerInfo();
    }

}
