﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseEffect : MonoBehaviour {

    // Use this for initialization
    public abstract void ApplyEffect(Monster monster);
    public abstract void RemoveEffect(Monster monster);
    public abstract void ExtendDuration();

}
