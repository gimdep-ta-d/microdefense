﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowEffect : BaseEffect {
    public Monster monster;
    public int duration = 2;
    public int currentDuration;
    bool check = true;
    public float slowAmount = 25f;
    public float ogSpeed;
	// Use this for initialization
	void Start () {
        monster = GetComponent<Monster>();
        ogSpeed = monster.speed;
        currentDuration = duration;
        ApplyEffect(monster);
	}
	
	// Update is called once per frame
	void Update () {
        if(check)
            ReduceCounter();
        if (currentDuration <= 0)
            RemoveEffect(monster);
	}

    public override void ApplyEffect(Monster monster)
    {
        Debug.Log("Slow time!");
        monster.speed = ogSpeed * ((100f - slowAmount) / 100f);
        monster.GetComponentInChildren<SpriteRenderer>().color = new Color(0,0,1);
    }

    public override void RemoveEffect(Monster monster)
    {
        monster.speed = ogSpeed;
        monster.GetComponentInChildren<SpriteRenderer>().color = new Color(1, 1, 1);
        Debug.Log("Effect Removed");
        DestroyImmediate(this);
    }

    public override void ExtendDuration()
    {
        Debug.Log("Extend slow");
        currentDuration = duration;
    }

    public void ReduceCounter()
    {
        currentDuration--;
        StartCoroutine("Wait");
        check = false;
    }

    public IEnumerator Wait()
    {
        yield return new WaitForSeconds(1f);
        check = true;
    }
}
