﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public List<Monster> monsterPrefabs;
    public List<Monster> bossPrefabs;
    public WaypointController wc;

    public int healthUpgrage = 30;
    public int powerSpike = 5;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SpawnBoss(int index, int wave) {
        Monster bossClone = Instantiate(
            bossPrefabs[index],
            new Vector3(transform.position.x, transform.position.y, -0.2f),
            Quaternion.identity
        );
        bossClone.waypoints = new Queue<Waypoint>(wc.waypoints);
        float health = getNewHealth(wave, bossClone.defaultMaxHealth);
        bossClone.SetStartHealth(health);
    }

    public void Spawn(int index, int wave) {
        Monster monsterClone = Instantiate(
            monsterPrefabs[index], 
            new Vector3(transform.position.x, transform.position.y, -0.2f), 
            Quaternion.identity
        );
        monsterClone.waypoints = new Queue<Waypoint>(wc.waypoints);
        float health = getNewHealth(wave, monsterClone.defaultMaxHealth);
        monsterClone.SetStartHealth(health);
    }

    private float getNewHealth(int wave, float defaultHealth) {
        if (wave < powerSpike) {
            return defaultHealth;
        } else {
            return enemyHealthFunction(wave, defaultHealth);
        }
    }

    private float enemyHealthFunction(int wave, float defaultHealth) {
        return defaultHealth + wave * (healthUpgrage + (int) (wave / 5));
    }
}
