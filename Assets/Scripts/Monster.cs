﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Monster : MonoBehaviour, ITargetable {

    public float speed;
    public float maxHealth;
    public float currentHealth;
    public float defaultMaxHealth = 100f;
    public float modifierx;
    public float modifiery;
    public int energyDrop;
    public int damage = 1;
    public SpriteRenderer healthbar;
    public Queue<Waypoint> waypoints;
    public Waypoint currentWaypoint;
    public bool isFacingRight = false;
    private int id;

	// Use this for initialization
	void Start () {
        currentHealth = maxHealth;
        id = new System.Random().Next(0, 0x7FFFFFFF);
        UpdateDirection();
	}

    public int GetId() {
        return id;
    }

    public bool IsAlive() {
        try {
            return gameObject != null;
        } catch (Exception e) {
            return false;
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (waypoints.Count <= 0)            
        {
            HealthManager.Instance.reduceHealth(damage);
            Destroy(gameObject);
        }
        else if (waypoints != null || waypoints.Peek() != null || waypoints.Count > 0)
        {
            
            transform.position = Vector3.MoveTowards(transform.position, waypoints.Peek().transform.position, Time.deltaTime * speed);
            if (transform.position == waypoints.Peek().transform.position)
            {
                waypoints.Dequeue();
                if(waypoints.Count > 0)
                    UpdateDirection();
            }
        }             
        healthbar.transform.localScale = new Vector3((currentHealth / maxHealth) * modifierx, modifiery, 1);
    }

    public void Damage(float amount)
    {
        currentHealth -= amount;
        if (currentHealth <= 0) {
            Die();
            //Debug.Log("Killed monster #" + id);
        }
    }

    void Die()
    {
        EnergyManager.Instance.GainEnergy(energyDrop);
        Destroy(gameObject);
    }
    
    void Flip()
    {
        isFacingRight = !isFacingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void UpdateDirection()
    {
        Vector3 destination = waypoints.Peek().transform.position;
        Vector3 dir = (destination - transform.position).normalized;
        //Debug.Log(dir);

        if (dir == Vector3.right && !isFacingRight || dir == Vector3.left && isFacingRight)
        {
            Flip();
        }
    }

    public float GetHealth() {
        return currentHealth;
    }

    public float GetSpeed() {
        return speed;
    }

    public Vector3 GetPosition() {
        return transform.position;
    }

    public void SetStartHealth(float health)
    {
        maxHealth = health;
        currentHealth = health;
    }
}

