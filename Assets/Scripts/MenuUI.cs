﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public abstract class MenuUI : MonoBehaviour
{

    public void ChangeScene(string sceneName)
    {
        Time.timeScale = 1f;
        //Debug.Log(Time.timeScale);
        SceneManager.LoadScene(sceneName);        
    }

    public abstract void ShowQuitCanvas();

    public abstract void HideQuitCanvas();

    public void QuitGame()
    {
        Application.Quit();
    }
}
