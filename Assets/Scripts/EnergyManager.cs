﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyManager : MonoBehaviour {

    public int recoverRate;
    public int startingEnergy;
    public Text energyText;
    public bool readyGainEnergy = true;
    public int energy;
    public WaveController waveController;

    static EnergyManager instance;
    public static EnergyManager Instance { get { return instance; } }

    // Use this for initialization
    void Start () {
        energy = startingEnergy;
        if (instance && instance != this) {
            Destroy(gameObject);
        } else {
            instance = this;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (readyGainEnergy && waveController.waveOnGoing) {
            energy += recoverRate;
            StartCoroutine(Wait());
            readyGainEnergy = false;
        }
        if (energyText)
            UpdateEnergyBar();

    }

    public IEnumerator Wait() {
        yield return new WaitForSeconds(1f);
        readyGainEnergy = true;
    }

    public void UpdateEnergyBar() {
        energyText.text = energy.ToString();
    }

    public void GainEnergy(int amount) {
        Debug.Log("=====================================energy gain: " + amount * (waveController.wave - 1));
        energy += (amount * (waveController.wave - 1))/2;
        UpdateEnergyBar();
    }

    public void UseEnergy(int amount) {
        energy -= amount;
        UpdateEnergyBar();
    }
}
