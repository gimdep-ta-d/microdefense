﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabContainer : MonoBehaviour {

    public List<Tower> towerPrefabs;
    static PrefabContainer instance;
    public static PrefabContainer Instance { get { return instance; } }
    // Use this for initialization
    void Start () {
        if (instance && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
