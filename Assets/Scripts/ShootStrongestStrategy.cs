﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts {
    class ShootStrongestStrategy : IShootingStrategy {
        public ITargetable GetTarget(List<ITargetable> shootables, Vector3 shootingPosition) {
            ITargetable currentTarget = shootables[0];
            for (int i = 1; i < shootables.Count; i++) {
                if (shootables[i].GetHealth() > currentTarget.GetHealth()) {
                    currentTarget = shootables[i];
                }
            }
            return currentTarget;
        }
    }
}
