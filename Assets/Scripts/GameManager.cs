﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public int TowerDamageModifier = 5;
    public GridManager grid;
    public EnemySpawner es;
    public WaveController waveC;
    public WaypointController waypointC;
    public bool isMovingTower;
    public Tower movingTower;
    static GameManager instance;
    public static GameManager Instance { get { return instance; } }
    public Collider2D col;
    public int protein;
    public int recoverRate;
    public Text energyText;
    public bool readyGainEnergy = true;
    public int towerDamageLevel;
    public int towerAttackSpeedLevel;

    // Use this for initialization
    void Awake () {
        if (instance && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        LoadPlayerData();
    }

    // Update is called once per frame
    void Update () {
        
	}

    public void MoveTower(Tower t)
    {
        isMovingTower = true;
        movingTower = t;
    }

    private void OnMouseUp()
    {
        if (grid.isMovingTower)
        {
            grid.isMovingTower = false;
            grid.selectedTower = null;
        }
            
        Debug.Log("You're clicking on a plane");
    }

    public void PlaceTower()
    {

    }

    public void LoadPlayerData()
    {
        Debug.Log("Loading...");
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            Debug.Log("File found...");
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

            towerDamageLevel = data.towerDamageLevel;
            towerAttackSpeedLevel = data.towerAttackSpeedLevel;
            protein = data.protein;
            Debug.Log("Save loaded!");
        }
        else
        {
            Debug.Log("No data found. Creating new save...");
            towerDamageLevel = 0;
            towerAttackSpeedLevel = 0;
            protein = 0;
        }
    }

    public void SavePlayerData()
    {
        Debug.Log("Saving...");
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

        PlayerData data = new PlayerData();
        data.towerDamageLevel = towerDamageLevel;
        data.towerAttackSpeedLevel = towerAttackSpeedLevel;
        data.protein = protein;

        bf.Serialize(file, data);
        file.Close();
    }

    public void DeletePlayerData()
    {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            File.Delete(Application.persistentDataPath + "/playerInfo.dat");
            LoadPlayerData();
        }
    }
}

[Serializable]
class PlayerData
{
    public int towerDamageLevel;
    public int towerAttackSpeedLevel;
    public int protein;
}
