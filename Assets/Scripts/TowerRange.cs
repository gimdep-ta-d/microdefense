﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerRange : MonoBehaviour {


    public Tower tower;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Monster")
        {
            Debug.Log("Found a monster");
            tower.AddTargetCandidate(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Monster")
        {
            Debug.Log("Monster left range");
            tower.RemoveTargetCandidate(collision.gameObject);
        }
    }

    private void OnMouseUp()
    {
        Debug.Log("You're clicking on a tower range collider");
    }
}
