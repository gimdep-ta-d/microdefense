﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverMenu : MenuUI {
    GameManager gameManager;
    CanvasGroup menuCanvasGroup;
    CanvasGroup quitCanvasGroup;


    public override void HideQuitCanvas()
    {
        throw new System.NotImplementedException();
    }

    public override void ShowQuitCanvas()
    {
        throw new System.NotImplementedException();
    }

    // Use this for initialization
    void Start () {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
