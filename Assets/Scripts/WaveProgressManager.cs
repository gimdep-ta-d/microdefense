﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveProgressManager : MonoBehaviour {

    public Image fillImage;
    public Text waveText;
    public Text startWaveText;
    public WaveController waveController;
    private float timer;
    private bool startWaveHelpGiven = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (waveController.wave == 1 && EnergyManager.Instance.energy != EnergyManager.Instance.startingEnergy) {
            if (timer > .2f) {
                timer = 0f;
                startWaveText.color = (startWaveText.color.Equals(Color.red) ? Color.black : Color.red);
            }
            if (!startWaveHelpGiven) {
                startWaveHelpGiven = true;
                InGameTextManager.Instance.ShowHelpText("Click \"Start Wave\" to tell our front-line defender that we are ready to face the bacteria/virus");
            }
            timer += Time.deltaTime;
        }
        if (waveController.wave == 2) {
            startWaveText.color = Color.black;
        }
	}

    public void changeColor(Color color) {
        fillImage.color = color;
    }

    public void changeText(string text) {
        waveText.text = text;
    }
}
