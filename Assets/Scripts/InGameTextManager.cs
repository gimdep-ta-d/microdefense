﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameTextManager : MonoBehaviour {

    static InGameTextManager instance;
    public GameObject towerInfoPanel;
    public GameObject towerBuildInfoPanel;
    public GameObject helperPanel;
    public Text towerNameText;
    public Text towerDamageText;
    public Text towerReloadText;
    public Text towerCloneCostText;
    public Text towerLevelUpCostText;
    public Text newTowerNameText;
    public Text newTowerInfoText;
    public Text helperText;

    public static InGameTextManager Instance { get { return instance; } }

    private Queue<string> helpTextQueue = new Queue<string>();
    private float helperTextTimer;
    private bool isShowingHelp = false;

    private static float helpTextDuration = 5f;

    // Use this for initialization
    void Start () {
        Debug.Log("InGameTextManager Start()");
        HideTowerInfo();
        HideBuildTowerInfo();
        HideHelperPanel();
        ShowHelpText("Select one of the cells on the left (the cuties) to be placed as your defense!");
	}

    // Use this for initialization
    void Awake() {
        Debug.Log("InGameTextManager Awake()");
        if (instance && instance != this) {
            Destroy(gameObject);
        }
        else {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update () {
		if (!isShowingHelp && helpTextQueue.Count > 0) {
            isShowingHelp = true;
            helperTextTimer = 0f;
            helperPanel.SetActive(true);
            helperText.text = helpTextQueue.Dequeue();
            Debug.Log("showing helptext: " + helperText.text);
        }
        if (isShowingHelp) {
            helperTextTimer += Time.deltaTime;
            if (helperTextTimer > helpTextDuration) {
                helperTextTimer = 0f;
                Debug.Log("hiding helptext: " + helperText.text);
                helperText.text = "";
                HideHelperPanel();
                isShowingHelp = false;
            }
        }
	}

    // text will be added to the queue
    public void ShowHelpText(string text) {
        helpTextQueue.Enqueue(text);
        Debug.Log("help text added: " + text);
    }

    private void HideHelperPanel() {
        helperPanel.SetActive(false);
    }

    public void UpdateTowerInfo(Tower selectedTower) {
        towerInfoPanel.SetActive(true);
        towerNameText.text = selectedTower.towerName + " Lv. " + selectedTower.level.ToString();
        towerDamageText.text = selectedTower.damage.ToString();
        towerReloadText.text = (selectedTower.rateOfFire * 1000).ToString() + " ms";
        towerCloneCostText.text = selectedTower.cloneCost.ToString();
        towerLevelUpCostText.text = selectedTower.levelUpCost.ToString();
    }

    public void HideTowerInfo() {
        towerInfoPanel.SetActive(false);
    }

    public void DisplayBuildTowerCost(Tower selectedTower) {
        towerBuildInfoPanel.SetActive(true);
        newTowerNameText.text = selectedTower.towerName;
        newTowerInfoText.text = "Cost: " + selectedTower.cost.ToString();
    }

    public void DisplayBuildTowerBuilding(Tower selectedTower) {
        towerBuildInfoPanel.SetActive(true);
        newTowerNameText.text = selectedTower.towerName;
        newTowerInfoText.text = "select grid";
    }

    public void DisplayBuildTowerInsufficientEnergy(Tower selectedTower) {
        towerBuildInfoPanel.SetActive(true);
        newTowerNameText.text = selectedTower.towerName;
        newTowerInfoText.text = "insufficient energy!";
    }

    public void HideBuildTowerInfo() {
        towerBuildInfoPanel.SetActive(false);
    }

}
