﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public interface ITargetable
    {
        int GetId();
        float GetHealth();
        float GetSpeed();
        Boolean IsAlive();
        Vector3 GetPosition();
    }
}
