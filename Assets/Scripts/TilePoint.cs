using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public struct TilePoint
{

    public int x;
    public int y;

    public TilePoint(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public static TilePoint operator +(TilePoint a, TilePoint b)
    {
        return new TilePoint(a.x + b.x, a.y + b.y);
    }

    public static TilePoint operator -(TilePoint p1, TilePoint p2)
    {
        return new TilePoint(p1.x - p2.x, p1.y - p2.y);
    }

    public static bool operator ==(TilePoint a, TilePoint b)
    {
        return a.x == b.x && a.y == b.y;
    }

    public static bool operator !=(TilePoint a, TilePoint b)
    {
        return !(a == b);
    }

    public override bool Equals(object obj)
    {
        if (obj is TilePoint)
        {
            TilePoint p = (TilePoint)obj;
            return x == p.x && y == p.y;
        }
        return false;
    }

    public bool Equals(TilePoint p)
    {
        return x == p.x && y == p.y;
    }

    public override int GetHashCode()
    {
        return x ^ y;
    }

    public override string ToString()
    {
        return string.Format("({0},{1})", x, y);
    }


}
