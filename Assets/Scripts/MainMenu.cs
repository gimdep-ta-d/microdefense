﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MenuUI {

    GameManager gameManager;
    Text proteinText;
    CanvasGroup menuCanvasGroup;
    CanvasGroup quitCanvasGroup;
    
    void Start()
    {
         gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
         proteinText = gameObject.transform.GetChild(1).GetComponent<Text>();
         if(proteinText)proteinText.text = gameManager.protein.ToString();

         Canvas[] canvas = GameObject.FindObjectsOfType<Canvas>();
         menuCanvasGroup = canvas[1].GetComponent<CanvasGroup>();
         quitCanvasGroup = canvas[0].GetComponent<CanvasGroup>();
        

        HideQuitCanvas();
    }

    public override void ShowQuitCanvas()
    {
        //reduce the visibility of normal UI, and disable all interraction
        menuCanvasGroup.alpha = 0.025f;
        menuCanvasGroup.interactable = false;
        menuCanvasGroup.blocksRaycasts = false;

        //enable interraction with confirmation gui and make visible
        quitCanvasGroup.alpha = 1;
        quitCanvasGroup.interactable = true;
        quitCanvasGroup.blocksRaycasts = true;
    }

    public override void HideQuitCanvas()
    {
        menuCanvasGroup.alpha = 1;
        menuCanvasGroup.interactable = true;
        menuCanvasGroup.blocksRaycasts = true;

        quitCanvasGroup.alpha = 0;
        quitCanvasGroup.interactable = false;
        quitCanvasGroup.blocksRaycasts = false;
    }

    public void ResetData()
    {
        GameManager.Instance.DeletePlayerData();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
