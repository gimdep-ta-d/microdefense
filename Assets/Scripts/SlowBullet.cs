﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowBullet : Bullet {

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("I hit a slowed monster");
        if (other.tag == "Monster" && other.GetComponent<Monster>().GetId() == target.GetId())
        {
            Debug.Log("Hit monster #" + target.GetId());
            other.gameObject.GetComponent<Monster>().Damage(damage);
            if (!other.gameObject.GetComponent<SlowEffect>())
            {
                other.gameObject.AddComponent<SlowEffect>();
            }
            else
            {
                other.gameObject.GetComponent<SlowEffect>().ExtendDuration();
            }

            gameObject.SetActive(false);
            return;
        }
    }
}
