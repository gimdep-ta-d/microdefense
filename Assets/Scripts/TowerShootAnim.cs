﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerShootAnim : MonoBehaviour {

    public Tower tower;
    bool test;
    Vector3 heading;

	// Use this for initialization
	void Start () {
        tower = GetComponentInParent<Tower>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    void Fire()
    {
        tower.FireEndFrame();
    }

    void Flip()
    {
        
    }
}
