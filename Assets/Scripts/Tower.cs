﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TowerType
{
    Basic,
    LongRange,
    RapidFire,
    Slow
}

public class Tower : MonoBehaviour {

    public int health;
    public int cost;
    public int levelLimit = 5;
    public int levelUpCost;
    public int cloneCost;
    public Tile tile;
    public int level;
    public Text levelText;
    public List<Button> buttons;
    public Bullet bulletPrefab;
    public List<ITargetable> targets;
    public ITargetable currentTarget;
    public Collider2D towerRange;
    public float baseDamage;
    public float damage;
    public float rateOfFire;
    public bool readyFire = true;
    public SpriteRenderer sr;
    public Animator anim;
    public bool isFacingRight = true;
    public TowerType towerType;
    public InGameTextManager textManager;
    public string towerName;

    public enum ShootingStrategyType {
        Farthest,
        Closest,
        Strongest,
        Weakest,
    }

    public ShootingStrategyType shootingStrategyType;

    private IShootingStrategy shootingStrategy;

	// Use this for initialization
	void Start () {
        textManager = InGameTextManager.Instance;
        levelLimit = 5;
        
        targets = new List<ITargetable>();
        switch (shootingStrategyType) {
            case ShootingStrategyType.Farthest:
                shootingStrategy = new ShootFarthestStrategy();
                break;
            case ShootingStrategyType.Closest:
                shootingStrategy = new ShootClosestStrategy();
                break;
            case ShootingStrategyType.Weakest:
                shootingStrategy = new ShootWeakestStrategy();
                break;
            case ShootingStrategyType.Strongest:
                shootingStrategy = new ShootStrongestStrategy();
                break;
            default:
                Debug.LogError("Invalid shootingStrategyType: " + shootingStrategyType);
                shootingStrategy = new ShootClosestStrategy();
                break;
        }

        sr = GetComponentInChildren<SpriteRenderer>();
	}

    private void Awake()
    {
        tile = GetComponentInParent<Tile>();
        UpdateDamage();
        UpdateCosts();
    }

    // Update is called once per frame
    void Update ()
    {
        ClearKilledTargets();
        if (targets.Count > 0 ) {
            Shoot();
        }
	}

    private void Shoot() {
        ITargetable shootTarget = shootingStrategy.GetTarget(targets, transform.position);
        currentTarget = shootTarget;
        Vector3 dir = currentTarget.GetPosition() - transform.position.normalized;
        if (readyFire) {
            if (dir.x < transform.position.x && isFacingRight || dir.x > transform.position.x && !isFacingRight)
            {
                Flip();
            }
            //Debug.Log("Firing at monster #" + shootTarget.GetId());
            anim.SetTrigger("Shoot");
            
            
        }
    }

    void Flip()
    {
        Transform sprite = GetComponentInChildren<TowerShootAnim>().transform;
        isFacingRight = !isFacingRight;

        Vector3 theScale = sprite.transform.localScale;
        theScale.x *= -1;
        sprite.transform.localScale = theScale;
    }

    public void FireEndFrame()
    {
        IShootable bulletClone;
        switch (towerType)
        {            
            case TowerType.Slow:
                bulletClone = ObjectPooler.Instance.GetPooledObject("SlowBullet").GetComponent<SlowBullet>();
                break;
            default:
                bulletClone = ObjectPooler.Instance.GetPooledObject("Bullet").GetComponent<Bullet>();
                break;
        }
        
        bulletClone.ShootAtFrom(currentTarget, transform.position, transform.rotation, damage);
        Debug.Log(bulletClone);
        StartCoroutine(Wait());
        readyFire = false;
    }


    private void ClearKilledTargets() {
        List<ITargetable> newTargets = new List<ITargetable>();
        for (int i = 0; i < targets.Count; i++) {
            if (targets[i].IsAlive()) {
                newTargets.Add(targets[i]);
            }
        }
        targets = newTargets;
    }

    private void OnMouseUp()
    {
        Debug.Log("Selecting tower on tile " + tile.pos);
        tile.gm.selectedTower = this;
        tile.gm.FindNeighbors(tile);
        textManager.UpdateTowerInfo(this);
    }

    public void DuplicateTower()
    {
        Debug.Log(tile);
        List<Tile> neighbors = tile.gm.FindNeighbors(tile);
        foreach(Tile spot in neighbors)
        {
            if (spot.CanPlaceTower() && level > 1 && cloneCost <= EnergyManager.Instance.energy)
            {
                Debug.Log("Clone tower!");
                int splitLevel = 0;
                if(level % 2 == 0)
                {
                    level /= 2;
                    splitLevel = level;
                }
                else
                {
                    splitLevel = level / 2;
                    level = splitLevel + 1;
                }
                levelText.text = level.ToString();
                Tower clone = Instantiate(PrefabContainer.Instance.towerPrefabs[(int)towerType], new Vector3(spot.transform.position.x, spot.transform.position.y, spot.transform.position.z - 0.1f), Quaternion.identity);
                //clone.transform.localScale = new Vector3(0.5f, 0.5f);
                clone.transform.SetParent(spot.transform);
                clone.level = splitLevel;
                clone.levelText.text = splitLevel.ToString();
                spot.t = clone;
                clone.tile = spot;
                clone.UpdateCosts();
                clone.UpdateDamage();
                EnergyManager.Instance.UseEnergy(cloneCost);
                UpdateCosts();
                UpdateDamage();
                textManager.UpdateTowerInfo(this);
                return;
            }
        }
    }

    public void AddTargetCandidate(GameObject gameObject)
    {
        if (gameObject.tag == "Monster") {
            targets.Add(gameObject.GetComponent<Monster>());
        }
    }

    public void RemoveTargetCandidate(GameObject gameObject) {
        for (int i = 0; i < targets.Count; i++) {
            if (gameObject.GetComponent<Monster>().GetId() == targets[i].GetId()) {
                targets.RemoveAt(i);
                return;
            }
        }
    }

    public void LevelUp()
    {
        if (levelUpCost <= EnergyManager.Instance.energy && level < levelLimit)
        {
            level++;
            EnergyManager.Instance.UseEnergy(levelUpCost);
            levelText.text = level.ToString();
            UpdateDamage();
            UpdateCosts();
            textManager.UpdateTowerInfo(this);
        }
    }

    public void UpdateDamage()
    {
        //formula for level damage
        damage = baseDamage + (Mathf.Floor(2.65f * level)) - 2 + Mathf.Floor(Mathf.Pow(Mathf.Pow(2, level), 0.5f)) + (GameManager.Instance.TowerDamageModifier * GameManager.Instance.towerDamageLevel) ; //floor root of power of two of level
    }

    public void UpdateCosts()
    {
        int l = level - 1;
        cloneCost = cost + (30 * l) + (l*l);
        levelUpCost = cloneCost - 15 + (30 * l);
    }

    public IEnumerator Wait()
    {
        yield return new WaitForSeconds(rateOfFire);
        readyFire = true;
    }

    /*void EnableButtons()
    {
        foreach (Button b in buttons)
        {
            b.gameObject.SetActive(true);
        }
    }

    void DisableButtons()
    {
        foreach (Button b in buttons)
        {
            b.gameObject.SetActive(false);
        }
    }*/
}
