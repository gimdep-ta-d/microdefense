﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public interface IShootingStrategy
    {
        ITargetable GetTarget(List<ITargetable> shootables, Vector3 shootingPosition);
    }
}
