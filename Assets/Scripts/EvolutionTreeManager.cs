﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class EvolutionTreeManager : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public static int MAX_LEVEL = 5;
    public static string DAMAGE = "DAMAGE";
    public static string ATTACK_SPEED = "ASPD";

    public string type;

    public GameObject panel;
    public Text proteinText;
    public Text currentLevelText;
    public Text upgradeCostText;
    public Text[] perLevelInfoText;

    public Button[] damageLevelButtons;
    public Button[] attackSpeedLevelButtons;

    public Button upgradeDamageButton;
    public Button upgradeAttackSpeedButton;

    public float[] damageLevel;
    public float[] attackSpeedLevel;

    public int[] damageUpgradeCost;
    public int[] attackSpeedUpgradeCost;

    // Use this for initialization
    void Start()
    {
        damageLevel = new float[]{ 5, 10, 15, 20, 25 };
        attackSpeedLevel = new float[]{ 0.05f, 0.10f, 0.15f, 0.20f, 0.25f };

        damageUpgradeCost = new int[] { 50, 70, 90, 110, 130 };
        attackSpeedUpgradeCost = new int[] { 50, 70, 90, 110, 130 };

        for (int i = 0; i < damageLevelButtons.Length; i++)
        {
            if (i < GameManager.Instance.towerDamageLevel)
            {
                damageLevelButtons[i].interactable = true;
            }
            else
            {
                damageLevelButtons[i].interactable = false;
            }
        }
        for (int i = 0; i < attackSpeedLevelButtons.Length; i++)
        {
            if (i < GameManager.Instance.towerAttackSpeedLevel)
            {
                attackSpeedLevelButtons[i].interactable = true;
            }
            else
            {
                attackSpeedLevelButtons[i].interactable = false;
            }
        }

        proteinText.text = GameManager.Instance.protein.ToString();
        UpdateUpgradeButtonsStatus();
        panel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void UpdateUpgradeButtonsStatus()
    {
        if (GameManager.Instance.towerDamageLevel == MAX_LEVEL)
        {
            upgradeDamageButton.GetComponent<Image>().color = new Color(161.0f / 255.0f, 108.0f / 255.0f, 43.0f / 255.0f);
        }

        if (GameManager.Instance.towerAttackSpeedLevel == MAX_LEVEL)
        {
            upgradeAttackSpeedButton.GetComponent<Image>().color = new Color(161.0f / 255.0f, 108.0f / 255.0f, 43.0f / 255.0f);
        }
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
    }

    public void UpgradeDamage()
    {
        //TODO: Add protein checker
        if (GameManager.Instance.towerDamageLevel < MAX_LEVEL && GameManager.Instance.protein >= damageUpgradeCost[GameManager.Instance.towerDamageLevel])
        {
            damageLevelButtons[GameManager.Instance.towerDamageLevel].interactable = true;
            GameManager.Instance.protein -= damageUpgradeCost[GameManager.Instance.towerDamageLevel];
            GameManager.Instance.towerDamageLevel++;            
            UpdateUpgradeButtonsStatus();

            currentLevelText.text = GameManager.Instance.towerDamageLevel.ToString();
            upgradeCostText.text = (GameManager.Instance.towerDamageLevel == MAX_LEVEL ? "-" : damageUpgradeCost[GameManager.Instance.towerDamageLevel].ToString());
            UpdateProteinText();
            GameManager.Instance.SavePlayerData();
        }
    }

    public void UpgradeAttackSpeed()
    {
        //TODO: Add protein checker
        if (GameManager.Instance.towerAttackSpeedLevel < MAX_LEVEL && GameManager.Instance.protein >= attackSpeedUpgradeCost[GameManager.Instance.towerDamageLevel])
        {
            attackSpeedLevelButtons[GameManager.Instance.towerAttackSpeedLevel].interactable = true;
            GameManager.Instance.protein -= damageUpgradeCost[GameManager.Instance.towerAttackSpeedLevel];
            GameManager.Instance.towerAttackSpeedLevel++;
            UpdateUpgradeButtonsStatus();

            currentLevelText.text = GameManager.Instance.towerAttackSpeedLevel.ToString();
            upgradeCostText.text = (GameManager.Instance.towerAttackSpeedLevel == MAX_LEVEL ? "-" : attackSpeedUpgradeCost[GameManager.Instance.towerAttackSpeedLevel].ToString());
            UpdateProteinText();
            GameManager.Instance.SavePlayerData();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (type == DAMAGE)
        {
            int currentLevel = GameManager.Instance.towerDamageLevel;
            currentLevelText.text = currentLevel.ToString();
            upgradeCostText.text = (GameManager.Instance.towerDamageLevel == MAX_LEVEL ? "-" : damageUpgradeCost[GameManager.Instance.towerDamageLevel].ToString());
            for (int i = 0; i < damageLevel.Length; i++)
            {
                perLevelInfoText[i].text = "+" + damageLevel[i].ToString("0") + " dmg";
            }
            panel.SetActive(true);
        }
        else if (type == ATTACK_SPEED)
        {
            int currentLevel = GameManager.Instance.towerAttackSpeedLevel;
            currentLevelText.text = currentLevel.ToString();
            upgradeCostText.text = (GameManager.Instance.towerAttackSpeedLevel == MAX_LEVEL ? "-" : attackSpeedUpgradeCost[GameManager.Instance.towerAttackSpeedLevel].ToString());
            for (int i = 0; i < attackSpeedLevel.Length; i++)
            {
                perLevelInfoText[i].text = "-" + attackSpeedLevel[i].ToString("0.00") + " s";
            }
            panel.SetActive(true);
        }
    }

    public void UpdateProteinText()
    {
        Text proteinText = GameObject.Find("Protein Text").GetComponentInChildren<Text>();
        proteinText.text = GameManager.Instance.protein.ToString();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        panel.SetActive(false);
    }
}
