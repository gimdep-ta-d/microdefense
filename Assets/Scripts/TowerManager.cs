﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerManager : MonoBehaviour {

    public static TowerManager Instance;


    public List<Tower> buildableTowers;
    public List<TowerButton> buttons;
    // Use this for initialization
	void Start () {
        Instance = this;
	}

    private void Awake()
    {
        int counter = 0;
        foreach (Tower t in buildableTowers)
        {
            buttons[counter].DesignateTower(t);
            counter++;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
